import time
import sys
import scrapper
import arg

#title_field = "/html/body/form/div[1]/input"
#message_field = "/html/body/form/div[2]/textarea"
#submit="/html/body/form/div[3]/input"


# Wait for the alert to be displayed and store it in a variable
def catch_alert():
	try:
		alert = WebDriverWait(driver,timeout=1).until(expected_conditions.alert_is_present())
		#print(alert)
	# Store the alert text in a variable
		text = alert.text
	# Press the OK button
		alert.accept()
	except:
	    print("No XSS detected")
	    scrapper.driver.quit()

	else :
		print("XSS found")

def read_wordlist(path):
	file = open(path, 'r')
	count = 0
	 
	while True:
	    count += 1
	 
	    # Get next line from file
	    line = file.readline()
	    insert_payload(line)
	    # if line is empty
	    # end of file is reached
	    if not line:
	        break
	    print("Line{}: {}".format(count, line.strip()))
	 
	file.close()


def insert_payload(payload, target_field):
	lenght = len(target_field)
	while i < lenght:
		driver.find_element_by_xpath(target_field[i]).send_keys(payload)
		i+=1
	driver.find_element_by_xpath(submit).click()

def main():
	try:
#		target_field_arg = arg.set_arg(*arg.target_field_param[])
		arg.set_arg()
#	payload_wordlist = set_optional_arg(wordlist_payload_param['name'],wordlist_payload_param['description'],wordlist_payload_param['help'], wordlist_payload_param['args'],wordlist_payload_param['type'])
#	buffer = set_optional_arg(buffer_param['name'],buffer_param['description'],buffer_param['help'],buffer_param['args'],buffer_param['type'])
#	submit=set_optional_arg(submit_param['name'],submit_param['description'],submit_param['help'], submit_param['args'],submit_param['type'])
#	read_wordlist()
		scrapper.start()
		catch_alert()
		pass
	except Exception as e:
		print("an error occur")
		scrapper.driver.quit()
		raise

main()





