from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
#options.add_argument('--headless')
driver = webdriver.Chrome("/opt/homebrew/bin/chromedriver", chrome_options=options)
def start():
	driver.get("http://challenge01.root-me.org/web-client/ch18/")
	page_source = driver.page_source
