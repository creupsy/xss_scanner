import argparse

dict_arg = {
    "target_field_param": {
        "name": "target_field",
                "alias": "tf",
                "usage": "require",
                "description": "XSS field tester",
                "help": "The targets fields have to be in xpath format \n ex : --target_field /html/body/form/div[1]/textarea",
                "args": "+",
                "type": str
     }
#     "buffer_param": {
#         "name": "buffer",
# :        "help": "fields that you want to fill it with random data to be allowed to send the form \n For example, if there is 3 field in the form and you only want to try injection in 1 field, however you have to fill all of them you can try somethin like that \n python3 main.py <the injected field> --buffer <non-injected-field1> <non-injected-field2> --buffer-value 0623327310 test@gmail.com",
#                 "args": "+",
#         "type": str
#     },
#     "submit_param": {
#         "name": "submit",
#         "alias": "s",
#         "usage": "require",
#         "description": "button xpath",
#                 "help": "The submit button have to be in xpath format \n ex : --submit /html/body/form/div[1]/input",
#                 "args": "1",
#         "type": str
#     },
#     "wordlist_payload_param": {
#         "name": "payload wordlist",
#         "description": "payload wordlist to test field",
#         "help": "The path to your payload wordlist",
#                 "args": "1",
#         "type": str
#     }
}


def set_arg():
    parser = argparse.ArgumentParser(description="description")
# FIX PARAM READING
    print(dict_arg["target_field_param"])  # LAST MODIF HERE OKOK APPLY LOOP TO READ NESTED DICT
#	parser.add_argument('integers', metavar='N', type=int, nargs='+',
#	                    help='an integer for the accumulator')
    for param, param_info in dict_arg.items():
        print("\nParametre :", param)
    #    for key in param_info:
    #        print(key + ':', param_info[key])
        print(param_info["name"])
        if param_info['usage'] == "optional":
            req = False
        elif param_info['usage'] == "require":
            req = True
        name = "--" + param_info['name']
        alias = '-' + param_info['alias']
        parser.add_argument(alias, name, type=param_info['type'], nargs=param_info['args'], required=req, help=param_info['help'])
    arg = parser.parse_args()
